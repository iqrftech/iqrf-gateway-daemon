News
====

Releases
--------

iqrf-gateway-daemon (2.0.0) RELEASED; urgency=medium

 [ Frantisek Mikulu ]
 [ Michal Konopa ]
 [ Roman Ondracek ]
 [ Rostislav Spinar ]

 * Requesting initial async packet from the coordinator if not received during boot
 * Monitoring initial async packet from the coordinator during runtime and setting RF mode
 * WriteTrConf service improved
 * BondNodeLocal and SmartConnect services improved
 * Setting hwpId for IQRF Sensor FRC fixed

 -- Rostislav Spinar <rostislav.spinar@iqrf.com>  Thu, 22 Nov 2018 12:00:00 +0000

Beta releases
-------------

iqrf-gateway-daemon (2.0.0-rc) testing; urgency=medium

 [ Frantisek Mikulu ]
 [ Michal Konopa ]
 [ Vasek Hanak ]
 [ Dusan Machut ]
 [ Vlastimil Kosar ]
 [ Roman Ondracek ]
 [ Jaromir Mastik ]
 [ Michal Valny ]
 [ Rostislav Spinar ]

 * IQRF JSON API v2, v1
 * IQRF Standard
 * IQRF Repository - offline/online
 * IQMESH Network services
 * MQ, MQTT, Websocket messaging
 * SPI, UART, CDC interfaces
 * DPA timing - unicast, broadcast, FRC
 * DPA 3.03, 3.02

 -- Rostislav Spinar <rostislav.spinar@iqrf.com>  Tue, 31 Oct 2018 11:20:00 +0000
